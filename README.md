# Darcula Stylish

Save your precious eyes from the white screen of death with this [Stylus](https://github.com/openstyles/stylus) theme for the Stylish editor. This changes the editor aesthetic as well as the default text theme, but still allows you to use the other built-in text themes if you prefer.

![screenshot](screenshot.jpg)
